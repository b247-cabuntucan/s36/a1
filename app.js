// Setup our dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 2001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// MongoDB Connection
mongoose.set('strictQuery', true); //for deprecation warnings
mongoose.connect('mongodb+srv://zuitt-bootcamp:password12345@zuitt-bootcamp.vgg4yb3.mongodb.net/activity-36?retryWrites=true&w=majority', {
    useNewUrlParser : true,
	useUnifiedTopology : true
});

mongoose.connection.once("open", () => console.log('Now connected to the database!'));

// Add the task route
app.use("/tasks", taskRoute); // By writing this, all the task routes would start with /tasks

// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));